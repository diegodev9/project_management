Rails.application.routes.draw do

  root to: 'projects#index'

  resources :projects do
    resources :tasks do
      member do
        patch :complete
      end
    end
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
