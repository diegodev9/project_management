class TasksController < ApplicationController
  before_action :set_project
  before_action :set_task, only: %i[destroy complete]

  def create
    @task = @project.tasks.create(task_params)

    redirect_to @project
  end

  def destroy
    flash[:notice] = if @task.destroy
                       'Task was deleted.'
                     else
                       'Task was not deleted.'
                     end
    redirect_to @project
  end

  def complete
    @task.update(completed_at: Time.zone.now)
    redirect_to @project, notice: 'Task Completed'
  end

  private

  def set_project
    @project = Project.find(params[:project_id])
  end

  def set_task
    @task = @project.tasks.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:content)
  end
end
